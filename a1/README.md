# LIS4331 - Advanced Mobile Web Application Development

## Brandon Senn 

### Assignment 1 Requirements:

*Course Work Links:*

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links: 1) This assignment and b) the completed tutorials above (bitbucketstationlocations)

### README.md file should include the following items:
* Screenshot of AMPPS Installation
* Screenshot of running Hello.java
* Screenshot of running Android Studio - My First App
* Screenshots of 'Contacts' app
* git commands w/ short descriptions
* Bitbucket repo links; a) This assignment b) BitbucketStationLocations

### Git Commands with Short Descriptions
1. git init - creates a new Git repo
2. git status - displays the state of the working directory and staging area
3. git add - adds files to the staging area for files
4. git commit - used to create a snapshot of the staged changes along a timeline of a Git project's history
5. git push - Used to upload local repository content to a remote repository
6. git pull - Used to download files from a remote repository
7. git rm - used to remove files from a git repository
    
### Assignment Screenshots

1. AMPPS install/PHP Information

![PHP Information](https://bitbucket.org/brandonsenn94/lis4381/raw/44cbd503bb4dcf68d0999529e97c40a7deaec005/A1/res/PHP%20info.png)

2. Hello.java

![Hello.java](https://bitbucket.org/brandonsenn94/lis4381/raw/44cbd503bb4dcf68d0999529e97c40a7deaec005/A1/res/Java%20installed%20helloworld.png)

3. Running Android Studio - My First App

![My First App](https://bitbucket.org/brandonsenn94/lis4381/raw/44cbd503bb4dcf68d0999529e97c40a7deaec005/A1/res/My%20First%20app.png)

4. Contacts App

| Home Page | Contact 1 |
| --- | --- |
| ![Home](./img/app_home.png) | ![First Contact](./img/app_contact1.png) |

| Contact 2 | Contact 3 |
| --- | --- |
| ![Contact 2](./img/app_contact2.png) | ![Contact 3](./img/app_contact3.png) |


### Tutorial Links: 
1. BitbucketTutorial - Station Locations [A1 BitbucketStationLocations](https://bitbucket.org/brandonsenn94/bitbucketstationlocations)

