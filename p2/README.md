# LIS4331 - Advanced Mobile Application Development

## Brandon Senn 

### Project 2 Requirements:
    * Basic CRUD functionality using SQLite Database

| Splash Screen | Original List |
| --- | --- |
| ![Splash Screen](img/splashScreen.png) | ![Original View List](img/viewOriginals.png) |

| Create Person (Before) | Create Person (After) |
| --- | --- |
| ![Before Adding](img/beforeAdding.png) | ![After](img/viewOriginals.png) |

| Update Person (1) | Update Person (2) | Update Person (3) |
| --- | --- | --- |
| ![Before](img/beforeUpdate.png) | ![After clicking update](img/afterUpdate.png) | ![After updating](img/afterUpdate_view.png) |

| Delete (Before)  | Delete (Viewing List) | 
| --- | --- |
| ![Before](img/beforeDelete.png) | ![Viewing List](img/afterDelete_View.png) |