# LIS4331 - Mobile Web Application Development

## Brandon Senn 

### Assignment 5 Requirements:

* Android app Screenshots (ItemsActivity, ItemActivity, Navigated to Article in default browser)
* Skillsets 13-15

| ItemsActivity | ItemActivity | Article Page in Browser | 
| --- | --- | --- |
| ![ItemsActivity](res/mainpage.png) | ![ItemActivity](res/afterclick.png) | ![Article](res/News.png) |

| Skillset 13 File | Opened File |
| --- | --- |
| ![Skillset 13 File](res/SS13_file.png) | ![Skillset 13 File Opened](res/SS13_fileOpened.png) |

| Skillset 14 | Skillset 15 |
| --- | --- |
| ![Skillset 14](res/SS14.png) | ![Skillset 15](res/SS15.png) |
