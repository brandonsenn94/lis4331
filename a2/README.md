# LIS4331 - Advanced Mobile Application Development

## Brandon Senn 

### Assignment 2 Requirements:

* Populated and Unpopulated TipCalculator app
* Skillsets 1-3 screenshots (LargestofTwoIntegers.java and ArraysAndLoops.java)

| Unpopulated App | Populated App |
| ------ | ------ |
| ![Unpopulated](img/unpopulated.png) | ![Populated](img/populated.png) |

| Skillset 1 (Circle.java) | Skillset 2 (MultipleNumber.java) | Skillset 3 (NestedStructures2.java) |
| ---- | ---- | ---- |
| ![Skillset1](img/SS1.png) | ![Skillset 2](img/SS2.png) | ![Skillset 3](img/SS3.png) |



