import java.util.Scanner;

class DecisionTree {
  public static void main(String[] args) {
    Scanner a = new Scanner(System.in);
    System.out.println("Developer: Brandon Senn");
    System.out.println("Program evaluates user entered characters.");
    System.out.println("Use following characters: W or w, C or c, H or h, N or n");
    System.out.println("Use following decision structures: if...else, and switch.\n"); 
    System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");
    System.out.print("Enter phone type: ");
    String b = a.nextLine();
    char c = b.charAt(0);
    System.out.println();
    //if...else
    if (c == 'W' || c == 'w'){
      System.out.println("if...else:");
      System.out.println("Phone type: work");
    }
    else if (c == 'C' || c == 'c'){
      System.out.println("if...else:");
      System.out.println("Phone type: cell");
    }
    else if (c == 'H' || c == 'h'){
      System.out.println("if...else:");
      System.out.println("Phone type: home");
    }
    else if (c == 'N' || c == 'n'){
      System.out.println("if...else");
      System.out.println("Phone type: none");
    }
    else{
      System.out.println("if...else:");
      System.out.println("Invalid character entry");
    }

    //switch
    switch(c){
      case 'W':
        System.out.println("switch:");
        System.out.println("Phone type: work");
        break;
      case 'w':
        System.out.println("switch:");
        System.out.println("Phone type: work");
        break;
      case 'C':
        System.out.println("switch:");
        System.out.println("Phone type: cell");
        break;
      case 'c':
        System.out.println("switch:");
        System.out.println("Phone type: cell");
        break;
      case 'H':
        System.out.println("switch:");
        System.out.println("Phone type: home");
        break;
      case 'h':
        System.out.println("switch:");
        System.out.println("Phone type: work");
        break;
      case 'N':
        System.out.println("switch:");
        System.out.println("Phone type: none");
        break;
      case 'n':
        System.out.println("switch:");
        System.out.println("Phone type: none");
        break;
      default:
        System.out.println("switch:");
        System.out.println("Incorrect character entry");
        break;
    }
  }
}