//Brandon Senn, BGS19C
//LIS4381 - Mobile Web App Dev

import java.util.Scanner;

class Main {
  public static void main(String[] args) {
    Scanner a = new Scanner(System.in);
    System.out.println("Program evaluates largest of two integers.");
    System.out.println("Note: Program does *NOT* check for non-numeric characters or non-integer values.\n");
    System.out.print("Enter first integer: ");
    int firstNumber = a.nextInt();
    System.out.print("Enter second integer: ");
    int secondNumber = a.nextInt();
    System.out.println("");
    if(firstNumber > secondNumber){
      System.out.println(firstNumber + " is larger than " + secondNumber);
    }
    else if (secondNumber > firstNumber){
      System.out.println(secondNumber + " is larger than " + firstNumber);
    }
    else{
      System.out.println("Integers are equal.");
    }
  }
}