class ArraysAndLoops {
  public static void main(String[] args) {
    System.out.println("Developer: Brandon Senn");
    System.out.println("Program loops through array of strings.");
    System.out.println("Use following values: dog, cat, bird, fish, insect.");
    System.out.println("Use following loop structures: for, enhanced for, while. Post test loop: do...while.\n");
    System.out.println("Note: Pretest loops: for, enhanced for, while. Posttest loop: do...while.");
    String[] animalLoops = {"dog", "cat", "bird", "fish", "insect"};

    System.out.println();
    System.out.println("For loop:");
    for(int i = 0; i < 5; i++){
      System.out.println(animalLoops[i]);
    }
    System.out.println();

    System.out.println("Enhanced for loop:");
    for(String a: animalLoops){
      System.out.println(a);
    }
    System.out.println();

    System.out.println("while loop:");
    int j = 0;
    while(j < 5){
      System.out.println(animalLoops[j]);
      j++;
    }

    System.out.println();
    System.out.println("do...while loop:");
    int k = 0;
    do{
      System.out.println(animalLoops[k]);
      k++;
    }
    while(k < 5);
  }
}