import java.util.Scanner;

public class MultipleNumber{
    public static void main(String[] args){
        System.out.println("Program determines if first number is a multiple of second, prints result");
        System.out.println("Example: 2, 4, 6, 8 are multiples of 2");
        System.out.println("1) Use integers. 2) Use printf() function to print.");
        System.out.println("Must *only* permit integer entry\n");

        Scanner input = new Scanner(System.in);
        int num1 = 0, num2 = 0;

        System.out.print("Enter Num 1: ");
        while(!input.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            input.next();
            System.out.print("Please try again. Enter Num 1: ");
        }
        num1 = input.nextInt();
        System.out.print("Num 2: ");
        num2= input.nextInt();
        System.out.println("");
        if(num1 % num2 == 0){
            int num3 = num1 / num2;
            System.out.println(num1 + " is a multiple of " + num2);
            System.out.println("The product of " + num3 + " and " + num2 + " is " + num1);
        }
        else{
            System.out.println(num1 + " is not a multiple of " + num2);
        }
    }
}