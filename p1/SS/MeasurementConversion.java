import java.util.Scanner;

public class MeasurementConversion{
    public static void main(String[] args){
        System.out.println("Program converts inches to centimeters, meters, feet, yards, and miles.");
        System.out.println("Notes: \n"+
                           "1) Use integer for inches. (Must validate integer input.\n" +
                           "2) Use printf() function to print(format values per below output.\n"+
                           "3) Create Java constants for the following values:\n"+
                           "\tInches to centimeters\n" +
                           "\tInches to meter\n"+
                           "\tInches to feet\n"+
                           "\tInches to yard\n"+
                           "\tFeet to mile");

        int inches=0;
        double centimeters=0.0, meters=0.0, feet=0.0, yards=0.0, miles=0.0;

        final double INCHES_TO_CENTIMETER=2.54;
        final double INCHES_TO_METER=0.0254;
        final double INCHES_TO_FOOT=12;
        final double INCHES_TO_YARD=36;
        final double FEET_TO_MILE=5280;
        
        Scanner input = new Scanner(System.in);

        System.out.print("Please enter number of inches: ");
        while(!input.hasNextInt()){
            System.out.println("Not valid integer!\n");
            input.next();
            System.out.print("Please enter inches: ");
        }
        inches = input.nextInt();
        

        centimeters=inches*INCHES_TO_CENTIMETER;
        meters=inches*INCHES_TO_METER;
        feet = inches/INCHES_TO_FOOT;
        yards= inches/INCHES_TO_YARD;
        miles = feet / FEET_TO_MILE;

        System.out.printf("%,d inch(es) equals\n\n%,.6f centimeter(s)\n%,.6f meter(s)\n%,.6f feet\n%,.6f yard(s)\n%,.8f mile(s)\n", inches, centimeters, meters, feet, yards, miles);


    }
}