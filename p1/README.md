# LIS4331 - Advanced Mobile Applications Development

## Brandon Senn 

### Project 1 Requirements:
- MyMusic Android App 
    - *Splash Screen Screenshot*
    - *Home Screen*
    - *Paused Song Screen* 
- Skillsets 7-9

--- 

### MyMusic App
|  Splash Screen  |  Home Screen  | Paused Screen |
|  ---  |  ---  | --- |
| ![Splash Screen](img/p1_splashScreen.png) | ![Home Screen](img/p1_openingScreen.png) | ![Pause Screen](img/p1_pauseScreen.png) |

---
### Skillsets
| Skillset 7 (Measurement Conversion) | Skillset 8 (Distance Calculator Unpopulated) (GUI)) | Skillset 8 (Distance Calculator Populated) |
|  ---  | --- | --- |
| ![Skillset 7](img/SS7.png) | ![Skillset 8 Unpopulated](img/SS8_empty.png) | ![Skillset 8 Populated](img/SS8_populated.png) |

--- 

| Skillset 9 (Unpopulated) | Skillset 9 (Single Interval) | Skillset 9 (Multiple Interval) |
| --- | --- | --- |
| ![Skillset 9 Unpopulated](img/SS9_unselected.png) | ![Skillset 9 (Single Interval)](img/SS9_singleInterval.png) | ![Skillset 9 (Multiple Interval)](img/SS9_MultipleInterval.png) |



