# LIS4381 - Mobile Web Application Development

## Brandon Senn 

### Assignment 4 Requirements:


#### Screenshots of Passing/Failing Data Validation
#### Carousel Screenshot
#### Skillset Screenshots

| Passing Validation | Failing Validation |
| --- | --- |
| ![PassedValidation](img/PassedValidation.png) | ![Failed validation](img/FailedValidation.png) | 

| Carousel Screenshot |
| --- |
| ![Carousel](img/Carousel.png)

| Skillset 10 | Skillset 11 | Skillset 12 |
| --- | --- | --- |
| ![SS10](img/SS10.png) | ![SS11](img/SS11.png) | ![SS12](img/SS12.png) |

## Local lis4381 web app:
http://localhost/repos/lis4381/index.php

