# Advanced Mobile Web Application Development

## Brandon Senn 

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](https://bitbucket.org/brandonsenn94/lis4331/src/master/a1/README.md)

    * Install Ampps
    * Screenshot of Running JDK Java Hello
    * Screenshot of running Android Studio My First App
    * Git commands w/ short descriptions
    * Bitbucket Repo links
        * This assignment 
        * Completed tutorial repo [BitbucketStationLocations](https://bitbucket.org/brandonsenn94/bitbucketstationlocations/src)    

2. [A2 README.md](https://bitbucket.org/brandonsenn94/lis4331/src/master/a2/README.md)
    * Skillset 1-3
    * Screenshots of Android App (Populated and Unpopulated)

3. [A3 README.md](https://bitbucket.org/brandonsenn94/lis4331/src/master/a3/README.md)
    * Currency Conversion App Screenshots
    * Skillsets 4-6

4. [A4 README.md](https://bitbucket.org/brandonsenn94/lis4331/src/master/a4/README.md)
    * Skillsets 10-12

5. [A5 README.md](https://bitbucket.org/brandonsenn94/lis4331/src/master/a5/README.md)
    * RSS Feed App 
    * Skillsets 13-15

6. [P1 README.md](https://bitbucket.org/brandonsenn94/lis4331/src/master/p1/README.md)
    * MyMusic App
    * Skillsets 7-9

7. [P2 README.md](https://bitbucket.org/brandonsenn94/lis4331/src/master/p2/README.md)
    * Basic CRUD Functionality with Android App using SQLite