import javax.swing.JOptionPane;

public class EvenOrOdd{
    public static void main(String[] args){
        String testNumString="";
        int num=0;

        JOptionPane.showMessageDialog(null, "Program uses Java GUI message and input dialogs.\n"+
                                      "Program evaluates integers as even or not.\n"+
                                      "Note: Program does not perform data validation");

        testNumString = JOptionPane.showInputDialog(null, "Enter integer", "Number Test Dialog", JOptionPane.INFORMATION_MESSAGE);

        num = Integer.parseInt(testNumString);

        if(num % 2 == 0){
            JOptionPane.showMessageDialog(null, num + " is an even number.");
        }
        else{
            JOptionPane.showMessageDialog(null, num + " is an odd number.");
        }
    }
}