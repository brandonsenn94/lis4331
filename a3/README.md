# LIS4331 - Advanced Mobile Web Application Development

## Brandon Senn 

### Assignment 3 Requirements:
- Currency Converter App (w/ Screenshots)
- Skillsets 4-6 (Time Conversion, Paint Calculator, Even Or Odd)

| App Splash Screen | App Toast Notification |
|   ---   | --- |
|![Splash Screen](img/a3_splashScreen.png) | ![Toast Notification](img/a3_ToastNotification.png) |

| Conversion (US to Euros) | Conversion (US to Pesos) | Conversion (US to Canadian) |
| --- | --- | --- |
| ![Euros](img/a3_Euros.png) | ![Pesos](img/a3_pesos.png) | ![Canadian](img/a3_Canadian.png) |


--- 

## Skillsets

| SS4 | 
| --- |
| ![SS4](img/SS4.png) |

| SS5 Message Prompt | SS5 Input Dialog | 
| --- | --- |
| ![SS5](img/SS5_MessagePrompt.png) | ![SS5 Input Prompt](img/SS5_InputDialog.png) |

| SS5 Even Number | SS5 Odd Number |
| --- | --- |
| ![SS5 Even Number](img/SS5_EvenNumber.png) | ![Odd Number](img/SS5_OddNumber.png) |

| SS6 Message Prompt | SS6 Paint Price Input | SS6 HeightInput |
| --- | --- | --- |
| ![SS6 Message Prompt](img/SS6_Dialog.png) | ![SS6 Price Input](img/SS6_PaintPriceInput.png) | ![SS6 Height Input](img/SS6_HeightInput.png) |

| SS6 Length Input | SS6 Width Input | SS6 Total Result |
| --- | --- | --- |
| ![SS6 Length Input](img/SS6_LengthInput.png) | ![SS6 Width Input](img/SS6_WidthInput.png) | ![Total Result](img/SS6_TotalPrompt.png) | 